export default class JsonConfig {
  constructor(data = {}) {
    this.data = {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      data
    }
  }
}
  