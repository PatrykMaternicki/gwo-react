import Datatable from '../components/Datatable'
import Form from '../components/Form'
import React from 'react'
import { connect } from 'react-redux'

const BasketContainer = ({ orders }) => {
    return <div>
        <Form />
        <Datatable orders={orders}/>
    </div>
}

const mapStateToProps = state => ({
    orders: state.order.list
})

export default connect(mapStateToProps, null)(BasketContainer)