import { connect } from 'react-redux'
import React, { useEffect } from 'react'
import { getAllBooks } from '../store/books/operations'
import Item  from '../components/Item'
import { CardColumns } from 'react-bootstrap'
const MainContainer = ({ books, getAllBooks }) => {

  useEffect(() => { getAllBooks() }, [])
  return <CardColumns>
    {books.map(book => <Item key={book.id} book={book} />)}
    </CardColumns>
}

const mapStateToProps = state => {
  return {
    books: state.books.list,
  }
}

const mapDispatchToProps = dispatch => ({
  getAllBooks: () => dispatch(getAllBooks())
})

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)