import { combineReducers } from 'redux';
import BooksReducer from './books';
import OrderReducer from './order';

const rootReducer = combineReducers({
  books: BooksReducer,
  order: OrderReducer
})

export default rootReducer;