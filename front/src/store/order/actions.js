import { default as TYPES } from './types';

const addOrder = item => ({
  type: TYPES.ADD_ORDER, item
});


export default {
  addOrder
}