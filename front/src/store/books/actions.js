import { default as TYPES } from './types';

const add = item => ({
  type: TYPES.ADD, item
});

const getAll = item => ({
  type: TYPES.GET_ALL, item
})

export default {
  add,
  getAll
}