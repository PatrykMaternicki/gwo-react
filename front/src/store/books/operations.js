import BooksClient from '../../clients/bookClient';
import Actions from './actions';

export const getAllBooks = () =>
  async (dispatch) => {
  const response = await BooksClient.get()
  response.data.map(item => dispatch(Actions.add(item)))
}