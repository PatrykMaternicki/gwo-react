import axios from 'axios'

export const get = async (url, config = {}) => {
  try {
    const response = await axios.get(url, config)
    return response
  } catch (error) {
    return error.response.data
  }
}

export const post = async (url, config = {}) => {
  try {
    const response = await axios.post(url, config)
    return response
  } catch (error) {
    return error.response
  }
}
