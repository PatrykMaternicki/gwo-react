import { get } from '../helpers/ajax';
import restConfig from '../models/restConfig'
import { restUrl } from '../dictionaries/rest'

export default class BookClient {
  static async get() {
    const response = await get(restUrl.book, new restConfig)
    return response.data
  }
}