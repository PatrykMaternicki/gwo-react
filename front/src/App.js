import React, { useEffect } from 'react'
import MainContainer  from './containers/Main'
import BasketContainer from './containers/Basket'
import { connect } from 'react-redux'
import Container from 'react-bootstrap/Container'
import Navbar from './components/Navbar'
import { getAllBooks } from './store/books/operations'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

const App = ({basketStock}) => {
  useEffect(() => { getAllBooks() }, [])
  return (
  <Container>
    <Navbar basketStock={basketStock}/>
  <Switch>
      <Route path="/" component={MainContainer} exact/>
      <Route path="/koszyk" component={BasketContainer} />
  </Switch>
    </Container>
  );
}

const mapStateToProps = state => {
  console.log(state)
  return {
    basketStock: state.order.list.length
  }
}

export default connect(mapStateToProps, null)(App)