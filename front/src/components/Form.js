import { Component } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

export default class FormOrder extends Component {

    static defaultProps = {
      error_msg: {
        types: {
          invalid: 'invalid',
          required: 'required'
        },
        zip_code: {
          required: 'Kod Pocztowy jest obowiązkowy',
          invalid: 'Kod pocztowy jest niepoprawny'
        }
      }
    }

    state = {
      validated: false,
      setValidated: false,
      first_name: '',
      last_name: '',
      city: '',
      zip_code: '',
      error_zip_code: false,
      error_last_name: false,
      error_first_name: false,
      error_type_zip_code: ''
    }

  handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({validated: true})
  }

  handleChange = (event) => {
    const state = this.state
    state[event.target.name] = event.target.value.trim()
    this.setState(state)
  }

  checkZipCode = (event) => {
    const test = /\d{2}-\d{3}/g.test(event.target.value) ? false : true
    const isMoreThenMax = event.target.value.length > 6
    const valid = test || isMoreThenMax ? true : false
    console.log(this.props)
    const type = this.props.error_msg.types
    valid && this.setState({error_zip_code_type: type.invalid })
    this.setState({error_zip_code: valid })
  }

  setMsg = (field) => {
    const isError = this.state[`error_${field}`]
    const msgType = this.state[`error_type_${field}`]
    console.log(isError)
    return ''
  }

  checkLength = (event, max, min) => {
    const { value, name} = event.target
    const state = this.state
    const isMoreThan = value.length >= max
    const isMinThan = value.length <= min
    const valid = isMoreThan || isMinThan
    state[`error_${name}`] = valid
    this.setState(state)  
  }

  checkRequire = (event) => {
    const { value, name} = event.target
    const valid = value.length > 0 ? false : true
    const type = this.props.error_msg.types
    valid && this.setState({[`error_type_${name}`]: type.required })
    this.setState({[`error_${name}`]: valid })
  }

  render() {
    const { validated, first_name, last_name, city, zip_code, error_zip_code, error_last_name, error_first_name }= this.state
    return(
        <Form noValidate validated={validated} onSubmit={this.handleSubmit}>
            <Form.Group controlId="formFirstName">
              <Form.Label>Imię</Form.Label>
                <Form.Control 
                  onChange={(event) => {
                    this.handleChange(event)
                    this.checkLength(event, 50, 4)
                  }} 
                  isInvalid={error_first_name}  
                  value={first_name} 
                  required 
                  type="text" 
                  name="first_name" 
                  placeholder="Podaj Imię" 
                />
              <Form.Control.Feedback type="invalid">Imię jest obowiązkowe</Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="formLastName">
                <Form.Label>Nazwisko</Form.Label>
                <Form.Control 
                  onChange={(event) => {
                    this.handleChange(event)
                    this.checkLength(event, 50, 5)
                  }} 
                  value={last_name}
                  isInvalid={error_last_name} 
                  name="last_name" 
                  required 
                  type="text" 
                  placeholder="Podaj Nazwisko" 
                />
                <Form.Control.Feedback type="invalid">Nazwisko jest obowiązkowe</Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="formCity">
                <Form.Label>Miejscowość</Form.Label>
                <Form.Control 
                  onChange={this.handleChange} 
                  value={city} 
                  name="city" 
                  required type="text"
                  laceholder="Podaj Miejscowość" 
                />
                <Form.Control.Feedback type="invalid">Miejscowość jest obowiązkowa</Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="formPostCode">
                <Form.Label>Kod pocztowy</Form.Label>
                <Form.Control 
                  onChange={(event) =>{
                    this.handleChange(event); 
                    this.checkRequire(event)
                    this.checkZipCode(event)
                  }}
                  isInvalid={error_zip_code} 
                  value={zip_code} 
                  name="zip_code" 
                  required 
                  type="text" 
                  placeholder="Podaj Kod Pocztowy"
                />
                <Form.Control.Feedback type="invalid">{this.setMsg('zip_code')}</Form.Control.Feedback>
            </Form.Group>
            <Button variant="primary" type="submit">
                Zamawiam i płacę
            </Button>
        </Form>
    )
  }
}