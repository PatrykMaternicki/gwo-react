
const baseUrl = `http://localhost:3001/api`;

export const restUrl = {
  book: `${baseUrl}/book`,
  order: `${baseUrl}/order`
}